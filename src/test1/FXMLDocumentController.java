/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test1;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

/**
 *
 * @author CSIE
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    private Connection conn;
    @FXML
    private void handleButtonAction(ActionEvent event) {
        try{
            Connection conn = DriverManager.getConnection("jdbc:derby://localhost:1527/contact", "test" , "test");
            doInset();
             doSelect();
              if(conn!=null)
            {
                label.setText("成功");
            }      
        } catch (SQLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            label.setText("fail");
        }
        
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
private int doInset(){
    int rows=0;
    String insert ="insert into TEST.COLLEAGIES values(?,?,?,?,?,?)";
    try{
    PreparedStatement stmt =conn.prepareStatement(insert);
    stmt.setInt(1,2);
    stmt.setString(2,"肉呆");
    stmt.setString(3,"五告");
    stmt.setString(4,"天兵二號");
    stmt.setString(5,"天兵組");
    stmt.setString(6,"test@test.com.tw");
    rows = stmt.executeUpdate();
    stmt.close();
    }catch (SQLException ex){
    Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE,null,ex);
    }finally{
      return rows;  
        }
    }
    private int doSelect() throws SQLException{
        int rows =0;
        String select="select";
        try{
            PreparedStatement stmt = conn.prepareStatement(select);
            ResultSet result=stmt.executeQuery();
            while (result.next()){
                System.out.println(result.getString(2));
                System.out.println(result.getString("TITLE"));
                rows++;
            }
            result.close();
            stmt.close();

        } catch (SQLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
        return rows;
        }
    }
}


                    

    
